#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "No action taken."
    echo "This script must be run as root"
    exit 1
fi

read -p "Enter desired hostname: " newHostname

echo "Generating new machine-id"
rm -f /var/lib/dbus/machine-id
rm -f /etc/machine-id
dbus-uuidgen --ensure=/etc/machine-id
ln -s /etc/machine-id /var/lib/dbus

echo "Generating new SSH Server keys"
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa -y
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa -y
ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa -b 521 -y

echo "Setting new Hostname"
sed -i "s/$HOSTNAME/$newHostname/g" /etc/hosts
sed -i "s/$HOSTNAME/$newHostname/g" /etc/hostname
hostnamectl set-hostname $newHostname

echo "Done!"

read -p "Would you like to reboot the system now? [y/n]: " confirm &&
    [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

    reboot
exit 0
